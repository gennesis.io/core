<?php
/**
 * Created by PhpStorm.
 * User: william
 * Date: 25/02/16
 * Time: 01:28
 */

namespace Apocalipse\Core\Helper;


use Apocalipse\Core\Flow\Wrapper;
use Apocalipse\Core\Model\Type\Warning;

/**
 * Class Console
 * @package Apocalipse\Core\Helper
 */
abstract class Console
{
    /**
     * @param $data
     */
    public static function log($data)
    {
        print '<pre>';
        var_dump($data);
        print '</pre>';
    }
}