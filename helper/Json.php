<?php
/**
 * Created by PhpStorm.
 * User: william
 * Date: 24/02/16
 * Time: 23:12
 */

namespace Apocalipse\Core\Helper;


use Apocalipse\Core\Flow\Wrapper;

abstract class Json
{
    /**
     * @param $mixed
     * @param int $options
     * @param int $depth
     * @return string
     */
    public static function encode($mixed, $options = 0, $depth = 512)
    {
        $string = json_encode($mixed, $options, $depth);

        switch (json_last_error()) {
            case JSON_ERROR_NONE:
                $error = '';
                break;
            case JSON_ERROR_DEPTH:
                $error = 'Maximum stack depth exceeded';
                break;
            case JSON_ERROR_STATE_MISMATCH:
                $error = 'Underflow or the modes mismatch';
                break;
            case JSON_ERROR_CTRL_CHAR:
                $error = 'Unexpected control character found';
                break;
            case JSON_ERROR_SYNTAX:
                $error = 'Syntax error, malformed JSON';
                break;
            case JSON_ERROR_UTF8:
                $error = 'Malformed UTF-8 characters, possibly incorrectly encoded';
                break;
            default:
                $error = 'Unknown error';
                break;
        }

        if ($error) {
            Wrapper::err('Json::encode error: ' . $error);
        }

        return $string;
    }

    /**
     * @param $string
     * @param bool $assoc
     * @param int $options
     * @param int $depth
     * @return mixed
     */
    public static function decode($string, $assoc = false, $options = 0, $depth = 512)
    {
        $mixed = json_decode($string, $assoc, $depth, $options);

        switch (json_last_error()) {
            case JSON_ERROR_NONE:
                $error = '';
                break;
            case JSON_ERROR_DEPTH:
                $error = 'Maximum stack depth exceeded';
                break;
            case JSON_ERROR_STATE_MISMATCH:
                $error = 'Underflow or the modes mismatch';
                break;
            case JSON_ERROR_CTRL_CHAR:
                $error = 'Unexpected control character found';
                break;
            case JSON_ERROR_SYNTAX:
                $error = 'Syntax error, malformed JSON';
                break;
            case JSON_ERROR_UTF8:
                $error = 'Malformed UTF-8 characters, possibly incorrectly encoded';
                break;
            default:
                $error = 'Unknown error';
                break;
        }

        if ($error) {
            Wrapper::err('Json::decode error: ' . $error);
        }

        return $mixed;
    }

    /**
     * @param $inArray
     * @return array|bool
     */
    public static function utf8($inArray)
    {
        static $depth = 0;

        /* our return object */
        $newArray = array();

        /* safety recursion limit */
        $depth++;
        if ($depth >= '30') {
            return false;
        }

        /* step through inArray */
        foreach ($inArray as $key => $val) {
            if (is_array($val) || is_object($val)) {
                /* recurse on array elements */
                $newArray[$key] = self::utf8($val);
            } else {
                /* encode string values */
                $newArray[$key] = utf8_encode($val);
            }
        }

        /* return utf8 encoded array */
        return $newArray;
    }

    /**
     * @param $string
     * @return bool
     */
    public static function isJson($string)
    {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }
}