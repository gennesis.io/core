<?php

namespace Apocalipse\Core;


use Apocalipse\Core\Helper\Json;
use Apocalipse\Core\Helper\Text;
use Apocalipse\Core\Helper\Tracker;
use Apocalipse\Core\Http\Header;
use Apocalipse\Core\Http\Request;
use Apocalipse\Core\Http\Response;
use Apocalipse\Core\Http\Service;
use Apocalipse\Core\Flow\Wrapper;
use Apocalipse\Core\Model\Type\Notification;
use Apocalipse\Core\Model\Type\Result;
use Kernel\Autoload\ClassLoader;

/**
 * Class App
 * @package Apocalipse\Core
 */
abstract class App
{
    /**
     * @var string
     */
    const SERVICES = 'services';

    /**
     * @var string
     */
    private static $type = 'overload';

    /**
     * @var bool
     */
    private static $log = false;

    /**
     * @var string
     */
    private static $address;

    /**
     * @var \stdClass
     */
    private static $environment;

    /**
     * @var bool
     */
    private static $responded = false;

    /**
     * @param $uri
     * @return string
     */
    public static function http($uri)
    {
        $dirty = explode('/', $uri);

        $route = [];
        foreach ($dirty as $key => $value) {
            if ($value && !in_array($value, ['.', '..'])) {
                $route[] = Text::clear($value);
            }
        }

        $method = strtoupper($_SERVER['REQUEST_METHOD']);

        $request = isset($route[0]) ? $route[0] : '';

        return self::resolve($method, $request, $route);
    }

    /**
     * @param $method
     * @param $request
     * @param $route
     * @return Response
     */
    protected static function resolve($method, $request, $route)
    {
        $resolve = null;

        $id = self::SERVICES . '.' . $method . '.' . $request;
        $service = config($id);

        if (is_null($service)) {

            $id = self::SERVICES . '.' . '*' . '.' . $request;
            $service = config($id);

            if (is_null($service)) {

                $id = self::SERVICES . '.' . $method . '.' . '*';
                $service = config($id);

                if (is_null($service)) {

                    $id = self::SERVICES . '.' . '*' . '.' . '*';
                    $service = config($id);
                }
            }
        }


        Wrapper::open();

        if (!is_null($service)) {

            $environment = null;
            if (isset($route[1])) {
                $env = $route[1];
                if (isset($service->environments) && is_array($service->environments)) {
                    if (in_array($env, $service->environments)) {
                        $environment = config($env,'environment');
                        array_splice($route, 1, 1);
                    }
                }
            }
            $environment = iif($environment, config('*','environment'));


            /** @noinspection PhpUndefinedFieldInspection */
            $use = pathToNamespace('/' . $service->use);

            ClassLoader::bootstrap($use);

            $load = $environment->scope->id;
            $environment->scope = ClassLoader::root(pathToNamespace('/' . $load));

            self::$environment = $environment;

            /** @noinspection PhpUndefinedFieldInspection */
            self::$type = $service->type;

            self::$log = sif($service, 'log', false);

            if (Tracker::isAllowed()) {

                /** @var Service $class */
                $class = new $use();

                $resolve = $class->render(new Request($method, $route));

            } else {

                self::output();
            }

        } else {

            Wrapper::push('No service found to request "' . Json::encode([$method, $request]) . '"');
        }

        Wrapper::close();

        return $resolve;
    }

    /**
     * @param $index
     * @param string $default
     * @param null $source
     * @return mixed
     */
    public static function input($index, $default = '', $source = null)
    {
        $request = $default;

        if (isset($_GET[$index]) && (is_null($source) || strtoupper($source) === 'GET')) {
            $request = $_GET[$index];
        } else if (isset($_POST[$index]) && (is_null($source) || strtoupper($source) === 'POST')) {
            $request = $_POST[$index];
        } else if (isset($_FILES[$index]) && (is_null($source) || strtoupper($source) === 'FILES')) {
            $request = $_FILES[$index];
        }

        return $request;
    }

    /**
     * @return string
     */
    public static function getUrlQueryString()
    {
        $fragments = explode('?', $_SERVER['REQUEST_URI']);

        array_shift($fragments);

        return implode('', $fragments);
    }

    /**
     * @param $requested
     * @return string
     */
    public static function getUrlRoot($requested)
    {
        $uri = explode('?', $_SERVER['REQUEST_URI'])[0];

        $root = Text::replaceLast($uri, $requested, '');
        if (Text::substring($root, Text::length($root) - 1, Text::length($root)) === '/') {
            $root = Text::substring($root, 0, Text::length($root) - 1);
        }

        return self::getUrlHost() . $root;
    }

    /**
     * @return string
     */
    public static function getUrlHost()
    {
        return (empty($_SERVER['HTTPS']) ? 'http' : 'https') . '://' . $_SERVER['HTTP_HOST'];
    }

    /**
     * @return bool
     */
    public static function isLog()
    {
        return self::$log;
    }

    /**
     * @return mixed
     */
    public static function getClientAddress()
    {
        if (!self::$address) {

            self::$address = $_SERVER['REMOTE_ADDR'];

            if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
                self::$address = $_SERVER['HTTP_CLIENT_IP'];
            } else if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                self::$address = $_SERVER['HTTP_X_FORWARDED_FOR'];
            }
        }

        return self::$address;
    }

    /**
     * @param $property
     * @return string
     */
    public static function environment($property)
    {
        return search(self::$environment, explode('.', $property));
    }

    /**
     * @param $response
     */
    public static function response($response)
    {
        if ($response instanceof Response) {

            $headers = $response->getHeaders();

            /** @var Header $header */
            foreach ($headers as $header) {
                header($header->getAssign(), $header->getReplace());
            }

            switch (self::$type) {

                case 'json/api':

                    $content = $response->getContent();
                    $ok = false;
                    if (!is_null($content)) {
                        $ok = true;
                    }
                    $status = Wrapper::getStatus();
                    if ($status === Wrapper::STATUS_UNDEFINED) {
                        $status = Wrapper::STATUS_SUCCESS;
                    }

                    $result = new Result($ok, $status, Wrapper::getNotifications(), $content);

                    echo $result->toJson();

                    break;

                default:

                    echo $response->getContent();
                    break;
            }

            self::$responded = true;
        }
    }

    /**
     *
     */
    public static function shutdown()
    {
        $error = error_get_last();

        if ($error['type'] === E_ERROR) {
            Wrapper::push($error['message'], Wrapper::STATUS_ERROR, $error['file'], $error['line']);
        }

        Tracker::shutdown(microtime());

        if (!Wrapper::isOk()) {

            if (!self::$responded) {
                self::output();
            }
        }

        if (Wrapper::$debug) {
            self::output();
        }
    }

    /**
     *
     */
    public static function output()
    {
        /**
         * @param $type
         */
        $error = function ($type) {

            ob_start();

            require_once __DIR__ . '/view/output.html.php';

            $content = ob_get_contents();

            ob_end_clean();

            self::response(new Response($content));
        };

        $error(self::$type);
    }

}