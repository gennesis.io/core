<?php
/*
 -------------------------------------------------------------------
 | @project: apocalipse
 | @package: Apocalipse\Core\Http
 | @file: Result.php
 -------------------------------------------------------------------
 | @user: william 
 | @creation: 03/04/16 10:29
 | @copyright: gennesis.io / arraysoftware.net
 | @license: MIT
 -------------------------------------------------------------------
 | @description:
 | PHP class
 |
 */

namespace Apocalipse\Core\Flow;


use Apocalipse\Core\Dao\Transaction;
use Apocalipse\Core\Helper\File;
use Apocalipse\Core\Helper\Json;
use Apocalipse\Core\Model\Type\Notification;
use Apocalipse\Core\Model\Type\Result;

/**
 * Class Result
 * @package Apocalipse\Core\Http
 */
abstract class Wrapper
{
    /**
     * @var string
     */
    public static $output_buffer;

    /**
     * @var \Apocalipse\Core\Model\Type\Result
     */
    private static $result;

    /**
     * @var string
     */
    const STATUS_UNDEFINED = 'undefined';

    /**
     * @var string
     */
    const STATUS_SUCCESS = 'success';

    /**
     * @var string
     */
    const STATUS_WARNING = 'warning';

    /**
     * @var string
     */
    const STATUS_ERROR = 'error';

    /**
     * @var string
     */
    const STATUS_INFO = 'info';

    /**
     * @var string
     */
    const STATUS_DATA = 'data';

    /**
     * @var string
     */
    const STATUS_UNAUTHORIZED = 'unauthorized';

    /**
     * @var bool
     */
    public static $debug = __APP_DEBUG__;

    /**
     *
     */
    public static function open()
    {
        ob_start();

        self::$result = new Result();
    }

    /**
     * @return bool
     */
    public static function close()
    {

        self::$output_buffer = ob_get_contents();

        ob_end_clean();

        self::$result->close();

        if (self::isOk()) {
            Transaction::commit();
        } else {
            Transaction::rollback();
        }

        return self::isOk();
    }

    /**
     * @param $var
     */
    public static function flush($var = null)
    {
        var_dump(iif($var, self::$result->getNotifications()));
        ob_flush();
    }

    /**
     * @param mixed $message
     * @param bool $trace
     */
    public static function stop($message, $trace = false)
    {
        switch (gettype($message)) {
            case 'string':
                $message = 'STOP: ' . $message;
                break;
            default:
                $message = print_r($message, true);
                break;
        }

        self::$result->push($message, Wrapper::STATUS_ERROR);

        self::$result->approve($trace ? print_r(debug_backtrace(), true) : $message);

        Wrapper::close();

        die();
    }

    /**
     * @return bool
     */
    public static function isClosed()
    {
        return self::$result->isClosed();
    }

    /**
     * @param $data
     */
    public static function approve($data)
    {
        self::$result->approve($data);
    }

    /**
     * @param $notifications
     */
    public static function append($notifications)
    {
        self::$result->append($notifications);
    }

    /**
     * @param $message
     * @param null $status
     * @param null $file
     * @param null $line
     */
    public static function push($message, $status = null, $file = null, $line = null)
    {
        self::$result->push($message, $status, $file, $line);
    }

    /**
     * @param $message
     * @param null $file
     * @param null $line
     */
    public static function info($message, $file = null, $line = null)
    {
        self::$result->push($message, Wrapper::STATUS_INFO, $file, $line = null);
    }

    /**
     * @param $message
     * @param null $file
     * @param null $line
     */
    public static function err($message, $file = null, $line = null)
    {
        self::$result->push($message, Wrapper::STATUS_ERROR, $file, $line);
    }

    /**
     * @param $message
     * @param null $file
     * @param null $line
     */
    public static function warn($message, $file = null, $line = null)
    {
        self::$result->push($message, Wrapper::STATUS_WARNING, $file, $line);
    }

    /**
     * @param $message
     * @param null $file
     * @param null $line
     */
    public static function data($message, $file = null, $line = null)
    {
        self::$result->push($message, Wrapper::STATUS_DATA, $file, $line);
    }

    /**
     * @param $message
     * @param null $file
     * @param null $line
     */
    public static function unauthorized($message, $file = null, $line = null)
    {
        self::$result->push($message, Wrapper::STATUS_UNAUTHORIZED, $file, $line);
    }

    /**
     * @return boolean
     */
    public static function isOk()
    {
        return self::$result ? self::$result->isOk() : false;
    }

    /**
     * @return string
     */
    public static function getStatus()
    {
        return self::$result->getStatus();
    }

    /**
     * @return array
     */
    public static function getBlacklist()
    {
        return self::$result->getBlacklist();
    }

    /**
     * @return string
     */
    public static function getData()
    {
        return self::$result->getData();
    }

    /**
     * @return array
     */
    public static function getNotifications()
    {
        return self::$result->getNotifications();
    }

    /**
     * @return string
     */
    public static function toJson()
    {
        return self::$result->toJson();
    }

    /**
     * @param $action
     * @param $data
     */
    public static function log($action, $data)
    {
        $status = 'ok';

        $messages = [];
        foreach (self::getNotifications() as $notification) {
            /** @var Notification $notification */
            if (in_array($notification->getStatus(), Wrapper::getBlacklist())) {
                $status = 'no';
            }
            $messages[] = ' - ' . $notification->getMessage();
        }

        $filename = path(true, 'temp', 'action', date('Y-m-d'), $status, date('His') . '-' . uniqid());

        $json = Json::encode(['action' => $action, 'data' => $data, 'messages' => $messages]);

        File::write($filename, $json);
    }

}