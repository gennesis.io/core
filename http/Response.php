<?php

namespace Apocalipse\Core\Http;


use Apocalipse\Core\Model\Type\Object;

/**
 * Class Response
 * @package Apocalipse\Core\Http
 */
class Response extends Object
{
    /**
     * @var string
     */
    private $content;

    /**
     * @var string
     */
    private $type;

    /**
     * @var array
     */
    private $headers;

    /**
     * Response constructor.
     * @param string $content
     * @param string $type
     * @param array $headers
     */
    public function __construct($content, $type = 'text/html', $headers = [])
    {
        parent::__construct();

        $this->content = $content;
        $this->type = $type;

        if (!is_array($headers)) {
            $headers = [];
        }
        $headers[] = new Header('Content-Type', $type);
        $headers[] = new Header('App-Name', config('app.name'));

        $this->headers = $headers;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return array
     */
    public function getHeaders()
    {
        return $this->headers;
    }

}