<?php
/*
 -------------------------------------------------------------------
 | @project: apocalipse
 | @package: Apocalipse\Core\Http
 | @file: Request.php
 -------------------------------------------------------------------
 | @user: william 
 | @creation: 17/03/16 04:13
 | @copyright: gennesis.io / arraysoftware.net
 | @license: MIT
 -------------------------------------------------------------------
 | @description:
 | PHP class
 |
 */

namespace Apocalipse\Core\Http;


use Apocalipse\Core\Domain\Content\Data;
use Apocalipse\Core\Domain\Data\Record;
use Apocalipse\Core\Helper\Json;

/**
 * Class Request
 * @package Apocalipse\Core\Http
 */
class Request
{
    /**
     * @var string
     */
    private $method;

    /**
     * @var array
     */
    private $route;

    /**
     * @var array
     */
    private $headers;

    /**
     * @var mixed
     */
    private $body;

    /**
     * Request constructor.
     * @param string $method
     * @param array $route
     * @param array $headers
     */
    public function __construct($method, array $route, array $headers = null)
    {
        $this->method = $method;
        $this->route = $route;

        if (is_null($headers)) {
            $headers = getAllHeaders();
        }
        $this->headers = $headers;

        if (isset($_GET['app-service'])) {
            unset($_GET['app-service']);
        }

        $post = new Record($_POST, false);

        $get = new Record($_GET, false);

        $files = new Record($_FILES, false);

        $payload = new Record([], false);

        $input = file_get_contents('php://input');
        if (Json::isJson($input)) {
            $payload = new Record(Json::decode($input), false);
        }

        $body = new Data($post, $get, $files, $payload);

        $_POST = array();
        $_GET = array();
        $_FILES = array();

        $this->body = $body;
    }

    /**
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @return array
     */
    public function getRoute()
    {
        return $this->route;
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @return array
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * @return Data
     */
    public function getBody()
    {
        return $this->body;
    }

}