<?php
/*
 -------------------------------------------------------------------
 | @project: apocalipse
 | @package: Apocalipse\Core\Dao
 | @file: Filter.php
 -------------------------------------------------------------------
 | @user: william 
 | @creation: 06/04/16 07:17
 | @copyright: gennesis.io / arraysoftware.net
 | @license: MIT
 -------------------------------------------------------------------
 | @description:
 | PHP class
 |
 */

namespace Apocalipse\Core\Dao;
use Apocalipse\Core\Flow\Wrapper;


/**
 * Class Filter
 * @package Apocalipse\Core\Dao
 */
class Filter
{
    /**
     * =    Equals: Selects elements that have the specified identifier with a value exactly equal to a certain value
     *
     * !=   Not Equals: Select elements that either don’t have the specified identifier, or do have the specified
     *      identifier but not with a certain value
     *
     * |=   Contains prefix: Selects elements that have the specified identifier with a value either equal to a given
     *      string or starting with that string followed by a hyphen (-)
     *
     * *=   Contains: Selects elements that have the specified identifier with a value containing a given substring
     *
     * ~=   Contains word: Selects elements that have the specified identifier with a value containing a given word,
     *      delimited by spaces
     *
     * $=   Ends with: Selects elements that have the specified identifier with a value ending exactly with a given
     *      string. The comparison is case sensitive
     *
     * ^=   Starts with: Selects elements that have the specified identifier with a value beginning exactly with a given
     *      string
     *
     * bt  Between: Selects elements that have the specified identifier with a value between a range
     *
     * in  In: Selects elements that have the specified identifier with a value in a valid list
     *
     *
     * @var string
     */
    public $operator = '=';

    /**
     * @var string
     */
    public $identifier = '';

    /**
     * @var mixed
     */
    public $value;

    /**
     * @var string
     */
    const LOGIC_CONECTOR_AND = 'AND';

    /**
     * @var string
     */
    const LOGIC_CONECTOR_OR = 'OR';

    /**
     * @var string
     */
    const OPERATOR_EQUAL = '=';

    /**
     * @var array
     */
    public static $LOGIC_OPERATORS = ['=', '!=', '|=', '*=', '~=', '$=', '^=', '>', '>=', '<', '<=', 'bti', 'btv', 'in', 'nin'];

    /**
     * Filter constructor.
     * @param string $operator
     * @param string $left
     * @param mixed $right
     */
    public function __construct($left, $right, $operator = null)
    {
        $default = Filter::OPERATOR_EQUAL;

        $this->identifier = $left;
        $this->value = $right;
        $this->operator = iif($operator, $default);

        if (!is_null($operator)) {
            if (!in_array($operator, Filter::$LOGIC_OPERATORS)) {
                $this->operator = $default;
                Wrapper::stop("Invalid Logic Operator. The operator '" . $operator . "' was modified to '" . $default . "'", true);
            }
        }
    }

}