<?php
/*
 -------------------------------------------------------------------
 | @project: apocalipse
 | @package: Apocalipse\Core\Dao
 | @file: Modifier.php
 -------------------------------------------------------------------
 | @user: william 
 | @creation: 03/04/16 21:51
 | @copyright: gennesis.io / arraysoftware.net
 | @license: MIT
 -------------------------------------------------------------------
 | @description:
 | PHP class
 |
 */

namespace Apocalipse\Core\Dao;


/**
 * Class Modifier
 * @package Apocalipse\Core\Dao
 */
class Modifier
{
    /**
     * @var array
     */
    private $filters;

    /**
     * @var array
     */
    private $sorters;

    /**
     * @var array
     */
    private $aggregators;

    /**
     * @var array
     */
    private $limiters;

    /**
     * @var array
     */
    private $joins;

    /**
     * Modifier constructor.
     * @param array $filters
     * @param array $sorters
     * @param array $aggregators
     * @param array $limiters
     * @param array $joins
     */
    public function __construct($filters = null, $sorters = null, $aggregators = null, $limiters = null, $joins = null)
    {
        $filters = iif($filters, []);
        if ($filters instanceof Filter) {
            $filters = [$filters];
        }

        $sorters = iif($sorters, []);
        if ($sorters instanceof Sorter) {
            $sorters = [$sorters];
        }

        $aggregators = iif($aggregators, []);
        if ($aggregators instanceof Aggregator) {
            $aggregators = [$aggregators];
        }

        /**
         * @adaptation to receive data via API
         */
        foreach ($filters as $key => $filter) {
            if (is_array($filter)) {
                $filters[$key] = new Filter(
                    sif($filter, 0), sif($filter, 1),
                    sif($filter, 2, Filter::OPERATOR_EQUAL), sif($filter, 3, Filter::LOGIC_CONECTOR_AND));
            }
        }

        $this->filters = $filters;
        $this->sorters = $sorters;
        $this->aggregators = $aggregators;

        $this->limiters = iif($limiters, []);
        $this->joins = iif($joins, []);
    }

    /**
     * @return array
     */
    public function getFilters()
    {
        return $this->filters;
    }

    /**
     * @param array $filters
     */
    public function setFilters($filters)
    {
        $this->filters = $filters;
    }

    /**
     * @return array
     */
    public function getSorters()
    {
        return $this->sorters;
    }

    /**
     * @param array $sorters
     */
    public function setSorters($sorters)
    {
        $this->sorters = $sorters;
    }

    /**
     * @return array
     */
    public function getAggregators()
    {
        return $this->aggregators;
    }

    /**
     * @param array $aggregators
     */
    public function setAggregators($aggregators)
    {
        $this->aggregators = $aggregators;
    }

    /**
     * @return array
     */
    public function getLimiters()
    {
        return $this->limiters;
    }

    /**
     * @param array $limiters
     */
    public function setLimiters($limiters)
    {
        $this->limiters = $limiters;
    }

    /**
     * @return array
     */
    public function getJoins()
    {
        return $this->joins;
    }

    /**
     * @param array $joins
     */
    public function setJoins($joins)
    {
        $this->joins = $joins;
    }

}
