<?php
/*
 -------------------------------------------------------------------
 | @project: apocalipse
 | @package: Apocalipse\Core\Dao
 | @file: Connection.php
 -------------------------------------------------------------------
 | @user: william
 | @creation: 03/04/16 09:35
 | @copyright: gennesis.io / arraysoftware.net
 | @license: MIT
 -------------------------------------------------------------------
 | @description:
 | PHP class
 |
 */

namespace Apocalipse\Core\Dao;

use Apocalipse\Core\Domain\Data\Record;
use Apocalipse\Core\Domain\Definition\Collection;
use Apocalipse\Core\Domain\Definition\Action;
use Apocalipse\Core\Flow\Wrapper;
use Apocalipse\Core\Model\Type\Result;

/**
 * Class Connection
 * @package Apocalipse\Core\Dao
 */
class DaoManager
{
    /**
     * @var String
     */
    private $driver;

    /**
     * Connection constructor.
     * @param $driver
     */
    public function __construct($driver)
    {
        $this->driver = $driver ? $driver : __APP_DEFAULT_DRIVER__;
    }

    /**
     * @param $collection
     * @param $record
     * @param $modifier
     * @param $debug
     *
     * @return string
     */
    public final function create(Collection $collection, Record $record, Modifier $modifier, $debug)
    {
        return $this->execute(Action::CREATE, TYPE_STRING, $collection, $record, $modifier, $debug);
    }

    /**
     * @param $collection
     * @param $fields
     * @param $modifier
     * @param $debug
     *
     * @return array
     */
    public final function read(Collection $collection, $fields, Modifier $modifier, $debug)
    {
        return $this->execute(Action::READ, TYPE_ARRAY, $collection, $fields, $modifier, $debug);
    }

    /**
     * @param $collection
     * @param $record
     * @param $modifier
     * @param $debug
     *
     * @return bool
     */
    public final function update(Collection $collection, Record $record, Modifier $modifier, $debug)
    {
        return $this->execute(Action::UPDATE, TYPE_BOOLEAN, $collection, $record, $modifier, $debug);
    }

    /**
     * @param $collection
     * @param $record
     * @param $modifier
     * @param $debug
     *
     * @return bool
     */
    public final function delete(Collection $collection, Record $record, Modifier $modifier, $debug)
    {
        return $this->execute(Action::DELETE, TYPE_BOOLEAN, $collection, $record, $modifier, $debug);
    }

    /**
     * @param $operation
     * @param $type
     * @param $collection
     * @param $record
     * @param $modifier
     * @param $debug
     *
     * @return mixed
     */
    private final function execute($operation, $type, $collection, $record, $modifier, $debug)
    {
        $data = null;

        /** @var Result $result */
        $result = Transaction::result($this->driver, $type, $operation, $collection, $record, $modifier, $debug);

        if ($result->isOk()) {

            $data = $result->getData();

        } else {

            Wrapper::append($result->getNotifications());
        }

        return $data;
    }

    /**
     * @param String $driver
     */
    public function setDriver($driver)
    {
        $this->driver = $driver;
    }

    /**
     * @return String
     */
    public function getDriver()
    {
        return $this->driver;
    }

}