<?php
/*
 -------------------------------------------------------------------
 | @project: apocalipse
 | @package: Apocalipse\Core\Dao
 | @file: Aggregator.php
 -------------------------------------------------------------------
 | @user: william 
 | @creation: 15/04/16 07:22
 | @copyright: fagoc.br / gennesis.io / arraysoftware.net
 | @license: MIT
 -------------------------------------------------------------------
 | @description:
 | PHP class
 |
 */

namespace Apocalipse\Core\Dao;


class Aggregator
{
    /**
     * @var array
     */
    public $rules = [];

    /**
     * Aggregator constructor.
     * @param array $rules
     */
    public function __construct($rules)
    {
        if (!is_array($rules)) {
            $rules = [$rules];
        }
        $this->rules = $rules;
    }

}