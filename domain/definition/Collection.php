<?php
/*
 -------------------------------------------------------------------
 | @project: apocalipse
 | @package: Apocalipse\Core\Domain
 | @file: Collection.php
 -------------------------------------------------------------------
 | @user: william 
 | @creation: 09/04/16 18:52
 | @copyright: fagoc.br / gennesis.io / arraysoftware.net
 | @license: MIT
 -------------------------------------------------------------------
 | @description:
 | PHP class
 |
 */

namespace Apocalipse\Core\Domain\Definition;


use Apocalipse\Core\Model\Type\Origin;

/**
 * Class Collection
 * @package Apocalipse\Core\Domain
 */
class Collection extends Origin
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $created_at;

    /**
     * @var string
     */
    private $changed_at;

    /**
     * @var string
     */
    private $created_by;

    /**
     * @var string
     */
    private $changed_by;

    /**
     * Collection constructor.
     * @param string $name
     * @param string $id
     */
    public function __construct($name, $id = null)
    {
        $this->name = $name;
        $this->id = iif($id ? $id : null, '_id');
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

}