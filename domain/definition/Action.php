<?php
/*
 -------------------------------------------------------------------
 | @project: apocalipse
 | @package: Apocalipse\Core\Domain
 | @file: ${FILE_NAME}
 -------------------------------------------------------------------
 | @user: william 
 | @creation: 08/04/16 18:18
 | @copyright: fagoc.br / gennesis.io / arraysoftware.net
 | @license: MIT
 -------------------------------------------------------------------
 | @description:
 | PHP class
 |
 */

namespace Apocalipse\Core\Domain\Definition;


use Apocalipse\Core\Model\Type\Origin;

/**
 * Class Operation
 * @package Apocalipse\Core\Domain
 */
abstract class Action extends Origin
{
    /**
     * @var string
     */
    const SAVE = 'save';

    /**
     * @var string
     */
    const CREATE = 'create';

    /**
     * @var string
     */
    const READ = 'read';

    /**
     * @var string
     */
    const UPDATE = 'update';

    /**
     * @var string
     */
    const DELETE = 'delete';
}