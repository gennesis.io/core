<?php
/*
 -------------------------------------------------------------------
 | @project: apocalipse
 | @package: Apocalipse\Core\Domain
 | @file: Field.php
 -------------------------------------------------------------------
 | @user: william 
 | @creation: 05/04/16 13:15
 | @copyright: gennesis.io / arraysoftware.net
 | @license: MIT
 -------------------------------------------------------------------
 | @description:
 | PHP class
 |
 */

namespace Apocalipse\Core\Domain\Definition;


/**
 * Class Field
 * @package Apocalipse\Core\Domain
 */
class Field extends Property
{
    /**
     * @var Authority
     */
    private $authority;

    /**
     * @var string
     */
    protected $behaviour;

    /**
     * @var string
     */
    protected $component;

    /**
     * @var Relationship
     */
    protected $relationship;

    /**
     * @var string
     */
    protected $default;

    /**
     * @var bool
     */
    protected $nullable;

    /**
     * Field constructor.
     * @param Authority $authority
     * @param string $name
     * @param string $type
     * @param bool $required
     * @param bool $unique
     * @param bool $create
     * @param bool $read
     * @param bool $update
     * @param bool $delete
     * @param int $length
     * @param string $behaviour
     * @param Relationship $relationship
     * @param string $component
     * @param string $default
     * @param bool $nullable
     */
    public function __construct(Authority $authority, $name, $type, $required = false, $unique = false, $create = true,
                                $read = true, $update = true, $delete = true, $length = null, $behaviour = null,
                                $relationship = null, $component = null, $default = null, $nullable = false)
    {
        parent::__construct($name, $type, $required, $unique, $create, $read, $update, $delete, $length);

        $this->behaviour = $behaviour;
        $this->relationship = $relationship;
        $this->component = $component;
        $this->default = $default;
        $this->nullable = $nullable;
    }

    /**
     * @return boolean
     */
    public function isNullable()
    {
        return $this->nullable;
    }

    /**
     * @param boolean $nullable
     */
    public function setNullable($nullable)
    {
        $this->nullable = $nullable;
    }

    /**
     * @return Authority
     */
    public function getAuthority()
    {
        return $this->authority;
    }

    /**
     * @param Authority $authority
     */
    public function setAuthority($authority)
    {
        $this->authority = $authority;
    }

    /**
     * @return string
     */
    public function getBehaviour()
    {
        return $this->behaviour;
    }

    /**
     * @param string $behaviour
     */
    public function setBehaviour($behaviour)
    {
        $this->behaviour = $behaviour;
    }

    /**
     * @return string
     */
    public function getComponent()
    {
        return $this->component;
    }

    /**
     * @param string $component
     */
    public function setComponent($component)
    {
        $this->component = $component;
    }

    /**
     * @return Relationship
     */
    public function getRelationship()
    {
        return $this->relationship;
    }

    /**
     * @param boolean $relationship
     */
    public function setRelationship($relationship)
    {
        $this->relationship = $relationship;
    }

    /**
     * @return string
     */
    public function getDefault()
    {
        return $this->default;
    }

    /**
     * @param string $default
     */
    public function setDefault($default)
    {
        $this->default = $default;
    }

}