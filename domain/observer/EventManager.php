<?php
/*
 -------------------------------------------------------------------
 | @project: apocalipse
 | @package: Apocalipse\Core\Domain\Observer
 | @file: EventManager.php
 -------------------------------------------------------------------
 | @user: william 
 | @creation: 21/04/16 00:13
 | @copyright: fagoc.br / gennesis.io / arraysoftware.net
 | @license: MIT
 -------------------------------------------------------------------
 | @description:
 | PHP class
 |
 */

namespace Apocalipse\Core\Domain\Observer;
use Apocalipse\Core\Domain\Data\Record;
use Apocalipse\Core\Flow\Wrapper;


/**
 * Class EventManager
 * @package Apocalipse\Core\Domain\Observer
 */
abstract class EventManager
{
    /**
     * @param Event $event
     * @param string $on
     * @param string $action
     * @param Record $record
     * @return mixed
     */
    public static function dispatch(Event $event, $on, $action, Record $record)
    {
        $dispatch = true;

        if ($event->getOn() === $on && in_array($action, $event->getActions())) {

            $use = pathToNamespace('/' . $event->getUse());

            if (class_exists($use)) {

                /** @var EventHandler $instance */
                $instance = new $use();

                $dispatch = $instance->call($event->getMethod(), $action, $record);
            } else {

                Wrapper::err('Class "' . $use . '" not found');
            }
        }

        return $dispatch;
    }
}