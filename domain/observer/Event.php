<?php
/*
 -------------------------------------------------------------------
 | @project: apocalipse
 | @package: Apocalipse\Core\Domain\Observer
 | @file: Event.php
 -------------------------------------------------------------------
 | @user: william 
 | @creation: 21/04/16 00:02
 | @copyright: fagoc.br / gennesis.io / arraysoftware.net
 | @license: MIT
 -------------------------------------------------------------------
 | @description:
 | PHP class
 |
 */

namespace Apocalipse\Core\Domain\Observer;


use Apocalipse\Core\Model\Type\Origin;

/**
 * Class Event
 * @package Apocalipse\Core\Domain\Observer
 */
class Event extends Origin
{
    /**
     * @var string
     */
    private $on;

    /**
     * @var array
     */
    private $actions;

    /**
     * @var string
     */
    private $use;

    /**
     * @var string
     */
    private $method;

    /**
     * Event constructor.
     * @param string $on
     * @param array $actions
     * @param string $use
     * @param string $method
     */
    public function __construct($on, array $actions, $use, $method)
    {
        $this->on = $on;
        $this->actions = $actions;
        $this->use = $use;
        $this->method = $method;
    }

    /**
     * @return string
     */
    public function getOn()
    {
        return $this->on;
    }

    /**
     * @param string $on
     */
    public function setOn($on)
    {
        $this->on = $on;
    }

    /**
     * @return array
     */
    public function getActions()
    {
        return $this->actions;
    }

    /**
     * @param array $actions
     */
    public function setActions($actions)
    {
        $this->actions = $actions;
    }

    /**
     * @return string
     */
    public function getUse()
    {
        return $this->use;
    }

    /**
     * @param string $use
     */
    public function setUse($use)
    {
        $this->use = $use;
    }

    /**
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @param string $method
     */
    public function setMethod($method)
    {
        $this->method = $method;
    }

}