<?php
/*
 -------------------------------------------------------------------
 | @project: apocalipse
 | @package: Apocalipse\Core\Domain\Observer
 | @file: EventHandler.php
 -------------------------------------------------------------------
 | @user: william 
 | @creation: 20/04/16 23:34
 | @copyright: fagoc.br / gennesis.io / arraysoftware.net
 | @license: MIT
 -------------------------------------------------------------------
 | @description:
 | PHP class
 |
 */

namespace Apocalipse\Core\Domain\Observer;


use Apocalipse\Core\Domain\Definition\Collection;
use Apocalipse\Core\Domain\ModelManager;

/**
 * Class EventHandler
 * @package Apocalipse\Core\Domain\Observer
 */
abstract class EventHandler extends ModelManager
{
    /**
     * EventHandler constructor.
     * @param Collection $collection
     * @param null $driver
     */
    public function __construct($collection = null, $driver = null)
    {
        parent::__construct($collection, $driver);
    }

    /**
     * @param $method
     * @param $action
     * @param $record
     *
     * @return bool
     */
    public final function call($method, $action, $record)
    {
        $call = null;

        if (method_exists($this, $method)) {

            $parameters = [$action, $record];

            $call = call_user_func_array([$this, $method], $parameters);
        }

        return $call;
    }
}