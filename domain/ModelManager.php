<?php
/*
 -------------------------------------------------------------------
 | @project: apocalipse
 | @package: Apocalipse\Core\Domain
 | @file: BusinessObject.php
 -------------------------------------------------------------------
 | @user: william 
 | @creation: 03/04/16 16:42
 | @copyright: gennesis.io / arraysoftware.net
 | @license: MIT
 -------------------------------------------------------------------
 | @description:
 | PHP class
 |
 */

namespace Apocalipse\Core\Domain;


use Apocalipse\Core\Dao\DaoManager;
use Apocalipse\Core\Dao\Filter;
use Apocalipse\Core\Dao\Modifier;
use Apocalipse\Core\Domain\Data\Record;
use Apocalipse\Core\Domain\Data\RecordSet;
use Apocalipse\Core\Domain\Definition\Authority;
use Apocalipse\Core\Domain\Definition\Behaviour;
use Apocalipse\Core\Domain\Definition\Collection;
use Apocalipse\Core\Domain\Definition\Field;
use Apocalipse\Core\Domain\Definition\Action;
use Apocalipse\Core\Domain\Definition\Relationship;
use Apocalipse\Core\Domain\Observer\Event;
use Apocalipse\Core\Domain\Observer\EventManager;
use Apocalipse\Core\Domain\Security\Auth;
use Apocalipse\Core\Helper\File;
use Apocalipse\Core\Flow\Wrapper;
use Apocalipse\Core\Helper\Text;
use Apocalipse\Core\Model\Dynamo;

/**
 * Class BusinessObject
 * @package Apocalipse\Core\Domain
 */
class ModelManager extends Authority
{
    /**
     * @var DaoManager
     */
    private $dao;

    /**
     * @var string
     */
    private $id = '_id';

    /**
     * @var array
     */
    private $items = [];

    /**
     * @var array
     */
    private $relationships = [];

    /**
     * @var array
     */
    private $listeners = [];

    /**
     * @var Modifier
     */
    protected $modifier;

    /**
     * @var array
     */
    protected $expanded = [];

    /**
     * ModelManager constructor.
     * @param Collection $collection
     * @param null $driver
     */
    public function __construct($collection, $driver = null)
    {
        parent::__construct($collection, $driver);

        $this->dao = new DaoManager($driver);
        $this->collection = $collection;
    }

    /**
     * @param string $driver
     */
    public function setDriver($driver)
    {
        parent::setDriver($driver);

        $this->dao = new DaoManager($driver);
    }

    /**
     * @param Record $record
     *
     * @return array
     */
    public final function save(Record $record)
    {
        $array = [];

        $id = $this->id;
        $reference = $record->get($id);

        $read = $this->read([new Filter($id, $reference)], null, null, null, [$id => '']);

        if ($read->size()) {

            $reference = $record->get($id);
            $save = $this->update($record, null);

        } else {

            if (!$reference) {
                $record->inject($id, uniqid());
            }
            $save = $this->create($record);
        }

        $relations = [];
        /** @var Relationship $relationship */
        foreach ($this->relationships as $key => $relationship) {
            $relations[$key] = $relationship->getReturned();
        }

        $array['id'] = $reference;// id
        $array['save'] = $save;// save
        $array['relations'] = (object)$relations;// relations

        return $array;
    }

    /**
     * @param Record $record
     *
     * @return string
     */
    public final function create(Record $record)
    {
        $create = null;

        if (!$record->indexOf($this->id)) {
            $record->inject($this->id, uniqid());
        }

        if ($this->defaultValidate(Action::CREATE, $record) && $this->validate(Action::CREATE, $record)) {

            if ($this->defaultBefore(Action::CREATE, $record) && $this->before(Action::CREATE, $record)) {

                /** @var Field $field */
                foreach ($this->items as $field) {

                    if (in_array($field->getBehaviour(), explode(',', Relationship::BEHAVIOURS))) {

                        $this->relationships[$field->getName()] = $this->relation($field, $record->get($field->getName()));
                    }
                    if (!$field->isCreatable()) {
                        $record->setPrivate($field->getName());
                    }
                }

                $modifier = new Modifier();

                $record->items($this->items);

                $record = $this->timestamps($record, true);

                $result = $this->dao->create($this->collection, $record, $modifier, $this->debug);

                $pks = $this->getPrimaryKeys($record, true);
                foreach ($pks as $name => $value) {
                    if ($record->indexOf($name)) {
                        $record->set($name, $result);
                    } else {
                        $record->inject($name, $result);
                    }
                }


                $snapshot = clone $record;
                if ($this->defaultAfter(Action::CREATE, $record) && $this->after(Action::CREATE, $record)) {

                    $create = $result;

                    if ($snapshot !== $record) {
                        $this->dao->update($this->collection, $record,
                            new Modifier(new Filter($this->id, $record->get($this->id))), $this->debug);
                    }
                } else {

                    $this->push(self::AFTER);
                }
            } else {

                $this->push(self::BEFORE);
            }
        } else {

            $this->push(self::VALIDATE);
        }

        return $create;
    }

    /**
     * @param null $filters
     * @param null $sorters
     * @param null $aggregators
     * @param null $limiters
     * @param null $fields
     *
     * @return RecordSet
     */
    public final function read($filters = null, $sorters = null, $aggregators = null, $limiters = null, $fields = null)
    {
        $data = [];

        if ($this->beforeRead()) {

            $joins = [];
            $filters = iif($filters, []);

            if ($filters instanceof Record) {

                $_filters = [];
                foreach ($filters->all() as $key => $value) {
                    $_filters[] = new Filter($key, $value);
                }
                $filters = $_filters;

            } else if ($filters instanceof Filter) {

                $filters = [$filters];
            }

            if ($this->modifier) {

                $defaultFilters = $this->modifier->getFilters();
                $filters = array_merge($filters ? $filters : [], $defaultFilters);

                $joins = $this->modifier->getJoins();

                if ($sorters === null) {
                    $sorters = $this->modifier->getSorters();
                } else {
                    $sorters = array_merge($sorters, $this->modifier->getSorters());
                }
            }
            // TODO: support default modifier to all modifiers

            if (!count($filters)) {
                $filters = [new Filter(1, 1)];
            }
            foreach ($filters as $key => $filter) {
                /** @var Filter $filter */
                if ($filter && isset($filter->identifier) && $filter->identifier === $this->id) {
                    $filter->identifier = $this->collection->getName() . '.' . $this->id;
                    $filters[$key] = $filter;
                }
            }

            if (is_null($fields) || $fields === '*') {

                $fields = [];

                /** @var Field $item */
                foreach ($this->items as $item) {
                    if (!in_array($item->getBehaviour(), explode(',', Relationship::BEHAVIOURS))) {
                        if ($item->isReadable()) {
                            $fields[$item->getName()] = '';
                        }
                    }
                }

                if (is_array($this->expanded)) {
                    foreach ($this->expanded as $key => $expansion) {
                        $id = $key;
                        $value = $expansion;
                        if (is_numeric($key)) {
                            $id = $expansion;
                            $value = '';
                        }
                        $fields[$id] = $value;
                    }
                }

            } else if (is_array($fields)) {

                foreach ($fields as $key => $value) {
                    if (is_numeric($key)) {
                        $fields[$value] = '';
                        unset($fields[$key]);
                    }
                }
            }

            $fields[$this->id] = '`' . $this->collection->getName() . '`' . '.' . '`' . $this->id . '`';

            $record = new Record($fields);

            $modifier = new Modifier($filters, $sorters, $aggregators, $limiters, $joins);

            $data = $this->afterRead($this->dao->read($this->collection, $record, $modifier, $this->debug));

        } else {

            $this->push(self::BEFORE);
        }

        return new RecordSet($data);
    }

    /**
     * @param Record $record
     * @param null $filters
     *
     * @return bool
     */
    public final function update(Record $record, $filters = null)
    {
        $update = null;

        if ($this->defaultValidate(Action::UPDATE, $record) && $this->validate(Action::UPDATE, $record)) {

            if ($this->defaultBefore(Action::UPDATE, $record) && $this->before(Action::UPDATE, $record)) {

                $reference = $record->get($this->id);

                if (is_null($reference)) {

                    $filters = iif($filters, []);

                } else {

                    $filters = [];
                    $filters[] = new Filter($this->id, $reference);
                }

                $this->relationships = [];

                /** @var Field $field */
                foreach ($this->items as $field) {

                    if (in_array($field->getBehaviour(), explode(',', Relationship::BEHAVIOURS))) {

                        $this->relationships[$field->getName()] = $this->relation($field, $record->get($field->getName()));
                    }

                    if (!$field->isUpdatable()) {
                        $record->setPrivate($field->getName());
                    }
                }

                if (count($filters)) {

                    $modifier = new Modifier($filters, [], []);

                    $record->items($this->items);

                    $record = $this->timestamps($record);

                    $result = $this->dao->update($this->collection, $record, $modifier, $this->debug);


                    $snapshot = clone $record;
                    if ($this->defaultAfter(Action::UPDATE, $record) && $this->after(Action::UPDATE, $record)) {

                        $update = $result;

                        if ($snapshot !== $record) {
                            $this->dao->update($this->collection, $record,
                                new Modifier(new Filter($this->id, $record->get($this->id))), $this->debug);
                        }
                    } else {

                        $this->push(self::AFTER);
                    }
                } else {

                    $this->push(self::FILTERS);
                }

            } else {

                $this->push(self::BEFORE);
            }
        } else {

            $this->push(self::VALIDATE);
        }

        return $update;
    }

    /**
     * @param Record $record
     * @param null $filters
     *
     * @return bool
     */
    public final function delete($record, $filters = null)
    {
        $delete = null;

        if ($this->defaultValidate(Action::DELETE, $record) && $this->validate(Action::DELETE, $record)) {

            if ($this->defaultBefore(Action::DELETE, $record) && $this->before(Action::DELETE, $record)) {

                $reference = $record->get($this->id);

                if (is_null($reference)) {

                    $filters = iif($filters, []);

//                    $pks = $this->getPrimaryKeys($record);
//                    foreach ($pks as $name => $value) {
//                        $filters[] = new Filter($name, $value);
//                    }
                } else {

                    $filters = [];
                    $filters[] = new Filter($this->id, $reference);
                }

                $this->relationships = [];

                /** @var Field $field */
                foreach ($this->items as $field) {

                    if (in_array($field->getBehaviour(), explode(',', Relationship::BEHAVIOURS))) {

                        $this->relationships[$field->getName()] = $this->relation($field, $record->get($field->getName()));
                    }

                    if (!$field->isDeletable()) {
                        $record->setPrivate($field->getName());
                    }
                }

                if (count($filters)) {

                    $modifier = new Modifier($filters, [], []);

                    $record->items($this->items);

                    $record = $this->timestamps($record);

                    $result = $this->dao->delete($this->collection, $record, $modifier, $this->debug);

                    if ($this->defaultAfter(Action::DELETE, $record) && $this->after(Action::DELETE, $record)) {

                        $delete = $result;
                    } else {

                        $this->push(self::AFTER);
                    }
                } else {

                    $this->push(self::FILTERS);
                }

            } else {

                $this->push(self::BEFORE);
            }
        } else {

            $this->push(self::VALIDATE);
        }

        return $delete;
    }

    /**
     * @param Field $field
     * @param $value
     * @return Relationship
     */
    private function relation(Field $field, $value)
    {
        $relation = $field->getRelationship();
        $relation->setValue($value);

        return $relation;
    }

    /**
     * @param $action
     * @param Record $record
     * @return bool
     */
    private final function defaultValidate($action, $record)
    {
        return true;
    }

    /**
     * @param $action
     * @param Record $record
     * @return bool
     */
    private final function defaultBefore($action, Record $record)
    {
        $dispatch = $this->dispatch('before', $action, $record);
        if (!$dispatch) {
            Wrapper::err(self::DISPATCH);
            return false;
        }

        return true;
    }

    /**
     * @param $action
     * @param Record $record
     * @return bool
     */
    private final function defaultAfter($action, Record $record)
    {
        $resolveRelationsDefault = $this->defaultResolveRelations($action, $record);
        if (!$resolveRelationsDefault) {
            Wrapper::err(self::RELATIONSHIP);
            return false;
        }

        $resolveRelations = $this->resolveRelations($action, $record);
        if (!$resolveRelations) {
            Wrapper::err(self::RELATIONSHIP);
            return false;
        }

        $dispatch = $this->dispatch('after', $action, $record);
        if (!$dispatch) {
            Wrapper::err(self::DISPATCH);
            return false;
        }

        /** @var Field $field */
        foreach ($this->getItems() as $field) {

            switch ($field->getType()) {

                case 'file':

                    $scope = environment('scope');
                    $root = path(__APP_DIR_ROOT__, $scope->type);
                    if ($scope->type === 'vendor') {
                        $root = path($root, $scope->package);
                    }
                    $resources = environment('resources');

                    $source = path($root, $resources->upload->root, $record->get($field->getName()));

                    if (File::exists($source)) {

                        $target = path($root, $resources->download->root, $record->get($field->getName()));

                        if ($this->debug) {

                            if (!File::copy($source, $target)) {
                                Wrapper::err("Can not copy file '" . $source . "' to '" . $target . "'");
                            }
                        } else {

                            if (!File::move($source, $target)) {
                                Wrapper::err("Can not move file '" . $source . "' to '" . $target . "'");
                            }
                        }
                    }
                    break;
            }
        }

        return true;
    }

    /**
     * @param $on
     * @param $action
     * @param $record
     * @return bool
     */
    private final function dispatch($on, $action, $record)
    {
        if (is_array($this->listeners) && count($this->listeners)) {

            foreach ($this->listeners as $event) {

                $dispatched = EventManager::dispatch($event, $on, $action, $record);
                if (is_null($dispatched)) {
                    Wrapper::err('Error while trying dispatch "' . $action . '" on "' . $on . '"');
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * @param $action
     * @param Record $record
     * @return bool
     */
    private final function defaultResolveRelations($action, Record $record)
    {
        $try = 0;
        $done = 0;

        /** @var Relationship $relation */
        foreach ($this->relationships as $key => $relation) {

            $try++;

            $behaviour = $relation->getBehaviour();
            $source = $relation->getSource();
            $target = $relation->getTarget();
            $value = $relation->getValue();
            $parameters = $relation->getParameters();

            switch ($behaviour) {
                case Behaviour::BELONGS_TO:

                    break;
                case Behaviour::HAS_MANY:

                    $reference = $record->get($source, true);

                    $returned = $this->hasMany($action, $target, $value, $reference, $parameters);
                    if (!is_null($returned)) {

                        $relation->setReturned($returned);

                        $this->relationships[$key] = $relation;
                        $done++;
                    }
                    break;
                case Behaviour::HAS_ONE:
                    break;
            }
        }

        return $try === $done;
    }

    /**
     * @param $action
     * @param $value
     * @param $target
     * @param $reference
     * @param $parameters
     *
     * @return bool
     */
    private final function hasMany($action, $target, $value, $reference, $parameters)
    {
        $returned = [];

        $try = 0;
        $done = 0;

        $records = (array)$value ? $value : [];

        $module = $parameters->module;
        $entity = $parameters->entity;

        $use = pathToNamespace('/' . path(environment('scope.id'), 'Model', $module, $entity));

        if (class_exists($use)) {

            $model = new $use();
        } else {

            $model = new Dynamo(camelCaseToDashes($module), camelCaseToDashes($entity), null, null, null);

            $model->skeleton();
        }

        $method = '';

        switch ($action) {
            case Action::SAVE:
            case Action::CREATE:
            case Action::UPDATE:
                $method = Action::SAVE;
                break;
            case Action::DELETE:
                $method = Action::DELETE;
                break;
        }

        if ($method) {

            $processeis = [];

            foreach ($records as $_record) {

                $try++;

                $array = (array)$_record;
                $array[$target] = $reference;

                $record = new Record($array);

                $processeis[] = $record->get($this->id);

                $return = $model->call($method, [$record]);

                if (!is_null($return)) {

                    $returned[] = $return;
                    $done++;
                }
            }

            $model->delete(new Record([]), [
                new Filter($target, $reference), Filter::LOGIC_CONECTOR_AND, new Filter($this->id, $processeis, 'nin')
            ]);
        }

        if ($try !== $done) {
            $returned = null;
        }

        return $returned;
    }

    /**
     * @param Record $record
     * @param bool $force
     * @return array
     */
    private final function getPrimaryKeys(Record $record, $force = false)
    {
        $pks = [];

        /** @var Field $item */
        foreach ($this->items as $item) {

            $name = $item->getName();
            //Wrapper::info([$name, $item->getBehaviour()]);
            if ($item->getBehaviour() === Behaviour::PK) {
                $value = $record->get($name);
                if ($value || $force) {
                    $pks[$name] = $value;
                }
            }
        }

        return $pks;
    }

    /**
     * @param Field $field
     * @return Field
     */
    public function add(Field $field)
    {
        return $this->items[$field->getName()] = $field;
    }

    /**
     * @param $action
     * @param Record $record
     * @return mixed
     */
    public function resolveRelations($action, Record $record)
    {
        return true;
    }

    /**
     * @param $action
     * @param Record $record
     * @return bool
     */
    public function validate($action, Record $record)
    {
        return true;
    }

    /**
     * @param $action
     * @param Record $record
     * @return bool
     */
    public function before($action, Record $record)
    {
        return true;
    }

    /**
     * @param $action
     * @param Record $record
     * @return bool
     */
    public function after($action, Record $record)
    {
        return true;
    }

    /**
     * @return bool
     */
    public function beforeRead()
    {
        return true;
    }

    /**
     * @param array $data
     * @return array
     */
    public function afterRead(array $data)
    {
        return $data;
    }

    /**
     * @param Record $record
     * @param bool $new
     * @return Record
     */
    public function timestamps(Record $record, $new = false)
    {
        $at = date('Y-m-d H:i:s');
        $by = Auth::isGuest() ? 'VISITANTE' : Text::upper(Auth::get('name'));

        $changes = ['_changed_at' => $at, '_changed_by' => $by];
        foreach ($changes as $key => $value) {
            if ($record->indexOf($key)) {
                $record->set($key, $value);
            } else {
                $record->inject($key, $value);
            }
        }

        if ($new) {

            $creates = ['_created_at' => $at, '_created_by' => $by];
            foreach ($creates as $key => $value) {
                if ($record->indexOf($key)) {
                    $record->set($key, $value);
                } else {
                    $record->inject($key, $value);
                }
            }
        }

        return $record;
    }

    /**
     * @return array
     */
    public final function getItems()
    {
        return $this->items;
    }

    /**
     * @param array $items
     */
    public final function setItems($items)
    {
        $this->items = $items;
    }

    /**
     * @return array
     */
    public final function getRelationships()
    {
        return $this->relationships;
    }

    /**
     * @param array $relationships
     */
    public final function setRelationships($relationships)
    {
        $this->relationships = $relationships;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return array
     */
    public function getListeners()
    {
        return $this->listeners;
    }

    /**
     * @return DaoManager
     */
    protected function getDao()
    {
        return $this->dao;
    }

    /**
     * @param array $listeners
     */
    public function setListeners($listeners)
    {
        $this->listeners = $listeners;
    }

    /**
     * @param $message
     * @param null $status
     */
    public final function push($message, $status = null)
    {
        Wrapper::push($message, $status);
    }

}