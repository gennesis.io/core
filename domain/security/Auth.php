<?php
/*
 -------------------------------------------------------------------
 | @project: apocalipse
 | @package: Apocalipse\Core\Domain\Auth
 | @file: Service.php
 -------------------------------------------------------------------
 | @user: william 
 | @creation: 07/04/16 09:50
 | @copyright: fagoc.br / gennesis.io / arraysoftware.net
 | @license: MIT
 -------------------------------------------------------------------
 | @description:
 | PHP class
 |
 */

namespace Apocalipse\Core\Domain\Security;


use Apocalipse\Core\Dao\Filter;
use Apocalipse\Core\Domain\Content\Container;
use Apocalipse\Core\Domain\Data\Record;
use Apocalipse\Core\Domain\Definition\Collection;
use Apocalipse\Core\Domain\ModelManager;
use Apocalipse\Core\Helper\File;
use Apocalipse\Core\Helper\Json;
use Apocalipse\Core\Http\Cookie;

/**
 * Class Auth
 * @package Apocalipse\Core\Domain\Auth
 */
class Auth
{
    /**
     * @var string
     */
    const TOKEN_AUTH = 'X-Auth-Token';
    /**
     * @var string
     */
    const TOKEN_NONCE = 'X-Nonce-Token';

    /**
     * @var string
     */
    const TOKEN = '_csrf';

    /**
     * @var bool
     */
    private static $allowed = false;

    /**
     * @var ModelManager
     */
    private static $model = null;

    /**
     * @return ModelManager
     */
    private static function model()
    {
        if (!self::$model) {

            $collection = environment('auth.collection');
            $driver = environment('auth.driver');

            self::$model = new ModelManager(new Collection($collection), $driver);
        }

        return self::$model;
    }

    /**
     * @return bool
     */
    public static function isGuest()
    {
        $guest = !self::$allowed;

        if ($guest) {
            $token = Cookie::get(self::TOKEN);

            if ($token) {
                $guest = !self::isAllowedAuth($token);
            }
        }

        return $guest;
    }

    /**
     * @param $token
     * @return bool
     */
    public static function isAllowedAuth($token = null)
    {
        $token = iif($token, Cookie::get(self::TOKEN));

        if (!self::$allowed) {
            self::$allowed = Session::check($token);
        }
        return self::$allowed;
    }

    /**
     * @param $nonce
     * @return bool
     */
    public static function isAllowedNonce($nonce)
    {
        if (!self::$allowed) {
            self::$allowed = Nonce::check($nonce);
        }
        return self::$allowed;
    }

    /**
     * @param $headers
     * @return bool
     */
    public static function isHeaderAllowed($headers)
    {
        $allowed = false;

        if (isset($headers[Auth::TOKEN_AUTH])) {

            $allowed = self::isAllowedAuth($headers[Auth::TOKEN_AUTH]);

        } else if (isset($headers[Auth::TOKEN_NONCE])) {

            $allowed = self::isAllowedNonce($headers[Auth::TOKEN_NONCE]);

        }

        return $allowed;
    }

    /**
     * @param $login
     * @param $password
     * @return Record
     */
    public static function login($login, $password)
    {
        $fields = environment('auth.fields');

        $auth_login = $fields->login;
        $auth_additionally = $fields->additional;
        $auth_password = $fields->password;

        $auth_name = $fields->name;
        $auth_email = $fields->email;
        $auth_role = $fields->role;

        $filter = [];
        $filter[] = new Filter($auth_login, addslashes($login));
        if (is_array($auth_additionally)) {
            foreach ($auth_additionally as $additional) {
                $filter[] = Filter::LOGIC_CONECTOR_OR;
                $filter[] = new Filter($additional, addslashes($login));
            }
        }

        $credential = null;

        //self::model()->debug = true;
        $recordSet = self::model()->read(
            $filter, null, null, null, [
            $auth_login => '', $auth_password => '', $auth_name => '', $auth_email => '', $auth_role => ''
        ]);

        if ($recordSet->size() === 1) {

            /** @var Record $record */
            foreach ($recordSet as $record) {
                if (Auth::match($password, $record->$auth_password)) {
                    $record->set($auth_password, null);
                    $credential = $record;
                }
            }
        }

        return $credential;
    }

    /**
     * @param string $token
     * @return bool
     */
    public static function logout($token = null)
    {
        return Session::destroy($token);
    }

    /**
     * @param $password
     * @return string
     */
    public static function crypt($password)
    {
        return crypt($password, '');
    }

    /**
     * @param $password
     * @param $candidate
     * @return bool
     */
    public static function match($password, $candidate)
    {
        return crypt($password, $candidate) === $candidate;
    }

    /**
     * @param $operation
     * @param Record $data
     * @param $headers
     * @return Container
     */
    public static function request($operation, Record $data, $headers)
    {
        $content = '';

        switch ($operation) {

            case 'login':

                if (!is_null($data->login) && !is_null($data->password)) {

                    $credential = Auth::login($data->login, $data->password);
                    if ($credential) {
                        $token = self::guid();
                        if (Session::create((object)$credential->all(), $token)) {
                            $content = $token;
                        }
                    }
                }
                break;

            case 'logout':

                if ($data->token) {

                    Session::destroy($data->token);
                }
                break;

            case 'permission':

                if (self::isHeaderAllowed($headers)) {

                    $token = $headers[Auth::TOKEN_AUTH];

                    $session = Session::read($token);
                    $auth_role = environment('auth.fields.role');

                    $role = $session->$auth_role;

                    $content = Json::decode(File::read(path(true, 'storage', 'perfil', $role . '.' . 'json')));
                }
                break;
        }

        return new Container($content, Container::TYPE_JSON);
    }

    /**
     * @param null $property
     * @param null $token
     * @return mixed|null
     */
    public static function get($property = null, $token = null)
    {
        $get = null;

        if (!$token) {
            if ($token = Cookie::get(self::TOKEN)) {
                $session = Session::read($token);
                $auth_property = environment('auth.fields.' . $property);
                $get = sif($session, $auth_property);
            }
        }

        return $get;
    }

    /**
     * @param bool $brackets
     * @return string
     */
    public static function guid($brackets = false)
    {
        mt_srand((double)microtime() * 10000);

        $charid = strtoupper(md5(uniqid(rand(), true)));
        $hyphen = chr(45);
        $uuid = substr($charid, 0, 8) . $hyphen . substr($charid, 8, 4) . $hyphen . substr($charid, 12, 4) . $hyphen .
            substr($charid, 16, 4) . $hyphen . substr($charid, 20, 12);
        if ($brackets) {
            $uuid = chr(123) . $uuid . chr(125);
        }

        return $uuid;
    }

}