<?php
/*
 -------------------------------------------------------------------
 | @project: apocalipse
 | @package: Apocalipse\Core\Domain\Content
 | @file: Container.php
 -------------------------------------------------------------------
 | @user: william 
 | @creation: 15/04/16 08:46
 | @copyright: fagoc.br / gennesis.io / arraysoftware.net
 | @license: MIT
 -------------------------------------------------------------------
 | @description:
 | PHP class
 |
 */

namespace Apocalipse\Core\Domain\Content;


class Container
{
    /**
     * @var string
     */
    private $type = '';

    /**
     * @var string
     */
    private $data = '';

    /**
     * @var array
     */
    private $info = [];

    /**
     * @var string
     */
    const TYPE_JSON = 'application/json';

    /**
     * @var string
     */
    const TYPE_HTML = 'text/html';

    /**
     * Container constructor.
     * @param $data
     * @param $type
     * @param array $info
     */
    public function __construct($data, $type, $info = [])
    {
        $this->type = $type;
        $this->data = $data;
        $this->info = $info;
    }

    /**
     * @return string
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param string $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return array
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * @param array $info
     */
    public function setInfo($info)
    {
        $this->info = $info;
    }

    /**
     * @param $name
     * @param $info
     */
    public function addInfo($name, $info)
    {
        $this->info[$name] = $info;
    }

}