<?php
/**
 * @package core\html
 * --------------------------------------------
 * @project ...: phptemplates
 * @user ......: william
 * @date ......: 25/02/16
 * @hour ......: 21:37
 * --------------------------------------------
 * @file ......: Renderer.php
 * @copyright .: .gennesis.io | .dracones.io
 * @license ...: MIT
 * --------------------------------------------
 */

namespace Apocalipse\Core\Domain\Content;


use Apocalipse\Core\Flow\Wrapper;
use Apocalipse\Core\Helper\File;
use Apocalipse\Core\Helper\Text;


class Renderer
{
    /**
     * @var string
     */
    private $views;

    /**
     * @var string
     */
    private $design;

    /**
     * @var Object
     */
    private $scope;

    /**
     * @var array
     */
    private $tags = [];

    /**
     * @var string
     */
    private $open = '[[';

    /**
     * @var string
     */
    private $close = ']]';

    /**
     * @var string
     */
    private $space = '( ?)+';

    /**
     * @var string
     */
    private $extensible = '<{{name}}>[[EXTENSIBLE]]</{{name}}>';

    /**
     * @var string
     */
    private $suffix;

    /**
     * Renderer constructor.
     * @param $views
     * @param $suffix
     * @param $design
     * @param $scope
     */
    public function __construct($views, $suffix, $design, $scope)
    {
        $this->views = $views;
        $this->suffix = $suffix;
        $this->design = $design;
        $this->scope = $scope;
    }

    /**
     * @param $template
     * @param bool $cache
     * @return mixed|string
     */
    public function compile($template, $cache = false)
    {
        $rendered = $this->process($this->getViewFileName($template), $cache);

        foreach ($this->tags as $tag) {
            $rendered = Text::replace($tag->content, $this->extensible($tag->name), $rendered);
        }

        return $rendered;
    }

    /**
     * @param $filename
     * @param $cache
     * @return string
     */
    private function process($filename, $cache)
    {
        $html = path(self::getCacheDirectory('html'), md5($filename) . '.html');

        if ($cache === 'yes' && File::exists($html)) {

            $rendered = File::read($html);

        } else {

            $rendered = $this->html($filename, $this->scope);

            if ($cache === 'yes') {

                File::write($html, $rendered);
            }
        }

        return $rendered;
    }

    /**
     * @param $dir
     * @return string
     */
    private static function getCacheDirectory($dir)
    {
        /**
         * @dir_dependency temp/cache
         */
        return path(true, 'temp', 'cache', $dir);
    }

    /**
     * @param $template
     * @return string
     */
    public function getViewFileName($template)
    {
        $name = pathToFile($template) . '.' . $this->suffix;

        return path($this->views, $name);
    }

    /**
     * @param $template
     * @return string
     */
    public function getDesignFileName($template)
    {
        $filename = pathToFile($template) . '.' . $this->suffix;

        if ($this->design) {
            $filename = path($this->design, $filename);
        }

        return $filename;
    }

    /**
     * @param $name
     */
    public function tag($name)
    {
        echo $this->extensible($name);
    }

    /**
     * @param $name
     * @return mixed
     */
    private function extensible($name)
    {
        return Text::replace($this->extensible, '{{name}}', strtolower($name));
    }

    /**
     * @param $template
     * @param $name
     */
    public function extend($template, $name)
    {
        $this->tags[] = (object)['name' => strtolower($name), 'content' => $this->append($template, false)];
    }

    /**
     * @param $page
     * @return mixed
     */
    public function import($page)
    {
        $filename = $this->getViewFileName($page);
        $scope = $this->scope;

        /** @noinspection PhpIncludeInspection */
        return include($filename);
    }

    /**
     * @param $template
     * @param bool $print
     * @return mixed|string
     */
    public function append($template, $print = true)
    {
        $filename = $this->getDesignFilename($template);

        $rendered = $this->process($filename, false);

        if ($print) {
            echo $rendered;
        }

        return $rendered;
    }

    /**
     * @param $filename
     * @param $scope
     * @return string
     */
    private function html($filename, $scope)
    {
        ob_start();

        /** @noinspection PhpIncludeInspection */
        include_once $filename;

        $response = ob_get_contents();

        ob_end_clean();

        return $response;
    }

    /**
     * @param $filename
     * @param $scope
     * @return mixed|string
     */
    private function output($filename, $scope)
    {
        $output = "";

        if (File::exists($filename)) {

            //$output = $this->scope(File::read($filename), $scope);

            //$output = $this->engine($output, $scope);

        } else {

            Wrapper::push('Error loading template file "' . $filename . '"');
        }

        return $output;
    }

    /**
     * @param $output
     * @param $scope
     * @return mixed
     */
    private function scope($output, $scope)
    {
        /**
         * @scope property
         */
        foreach ($scope as $property => $value) {

            $type = gettype($value);

            switch ($type) {
                case "boolean":
                case "integer":
                case "double":
                case "float":
                case "string":
                    $output = preg_replace('/' . preg_quote($this->open) . $this->space . '\$' . preg_quote($property) . $this->space . preg_quote($this->close) . '/', $value, $output);
                    break;
                case "array":
                case "object":
                    //return $this->scope($output, $scope);
                    $output = preg_replace('/' . preg_quote($this->open) . $this->space . '\$' . preg_quote($property) . $this->space . preg_quote($this->close) . '/', json_encode($value), $output);
                    break;
                case "resource":
                case "NULL":
                case "unknown type":
                    $output = preg_replace('/' . preg_quote($this->open) . $this->space . '\$' . preg_quote($property) . $this->space . preg_quote($this->close) . '/', '', $output);
                    break;
            }
        }

        return $output;
    }

    /**
     * @param $output
     * @param $scope
     * @return mixed
     */
    private function engine($output, $scope)
    {

        /**
         * @string embeds
         */
        if (preg_match_all("/" . preg_quote($this->open) . $this->space . "['\"](.*?)['\"]" . $this->space . preg_quote($this->close) . "/", $output, $all)) {
            $marks = $all[0];
            $values = $all[2];
            foreach ($marks as $i => $mark) {
                $output = preg_replace("/" . preg_quote($mark, "/") . "/", isset($values[$i]) ? $values[$i] : "", $output, 1);
            }
        }

        /**
         * @number embeds
         */
        if (preg_match_all("/" . preg_quote($this->open) . $this->space . "([0-9]+)" . $this->space . preg_quote($this->close) . "/", $output, $all)) {
            $marks = $all[0];
            $values = $all[2];
            foreach ($marks as $i => $mark) {
                $output = preg_replace("/" . preg_quote($mark, "/") . "/", isset($values[$i]) ? $values[$i] : "", $output, 1);
            }
        }

        /**
         * @method embeds
         */
        if (preg_match_all("/" . preg_quote($this->open) . $this->space . "\@(.*?)\((.*?)\)" . $this->space . preg_quote($this->close) . "/", $output, $all)) {

            //Console::log($all);
            $marks = $all[0];

            $method = $all[2][0];
            $parameters = array_merge([$scope], preg_split("/,/", $all[3][0]));

            foreach ($marks as $i => $mark) {
                $output = preg_replace("/" . preg_quote($mark, "/") . "/", call_user_func_array([$this, $method], $parameters), $output, 1);
            }
        }

        /**
         * @php functions
         */
        if (preg_match_all("/" . preg_quote($this->open) . $this->space . "php(.*?)" . $this->space . preg_quote($this->close) . "/", $output, $all)) {
            $marks = $all[0];
            $values = $all[2];
            foreach ($marks as $i => $mark) {
                ob_start();
                eval($values[$i] . ";");
                $ob = ob_get_contents();
                ob_end_clean();
                $output = preg_replace("/" . preg_quote($mark, "/") . "/", $ob, $output, 1);
            }
        }

        /**
         * @php print functions
         */
        if (preg_match_all("/" . preg_quote($this->open) . $this->space . "\=(.*?)" . $this->space . preg_quote($this->close) . "/", $output, $all)) {
            $marks = $all[0];
            $values = $all[2];
            foreach ($marks as $i => $mark) {
                ob_start();
                $return = "";
                eval('$return = ' . $values[$i] . ';');
                $ob = ob_get_contents();
                ob_end_clean();
                $output = preg_replace("/" . preg_quote($mark, "/") . "/", ($ob ? $ob : $return), $output, 1);
            }
        }

        /**
         * @clear
         */
        if (preg_match_all("/" . preg_quote($this->open) . $this->space . "(.*?)" . $this->space . preg_quote($this->close) . "/", $output, $all)) {
            $marks = $all[0];
            foreach ($marks as $i => $mark) {
                $output = preg_replace("/" . preg_quote($mark, "/") . "/", "", $output, 1);
            }
        }

        return $output;
    }

    /**
     * @param $name
     * @param $args
     * @return string
     */
    public function __call($name, $args)
    {
        $return = '';
        $page = $args[0];

        switch ($name) {
            case 'include':
                $return = $this->compile($page, $this->arg($args, 1));
                break;
            case 'asset':
                $return = asset($this->arg($args, 1));
                break;
            case 'url':
                $return = url($this->arg($args, 1));
                break;
        }

        return $return;
    }

    /**
     * @param $arguments
     * @param $index
     * @param null $default
     * @return string
     */
    private function arg($arguments, $index, $default = null)
    {
        $arg = $default;
        if (isset($arguments[$index])) {
            $arg = Text::substring($arguments[$index], 1, -1);
        }
        return $arg;
    }

}