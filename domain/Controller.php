<?php
/**
 * @package core\route\http
 * --------------------------------------------
 * @project ...: phptemplates
 * @user ......: william
 * @date ......: 25/02/16
 * @hour ......: 21:52
 * --------------------------------------------
 * @file ......: Controller.php
 * @copyright .: .gennesis.io | .dracones.io
 * @license ...: MIT
 * --------------------------------------------
 */

namespace Apocalipse\Core\Domain;


use Apocalipse\Core\Domain\Content\Container;
use Apocalipse\Core\Domain\Content\Data;

/**
 * Class Controller
 * @package Apocalipse\Core\Domain
 */
abstract class Controller
{
    /**
     * @var mixed
     */
    protected $context;

    /**
     * Controller constructor.
     * @param mixed $context
     */
    public function __construct($context = null)
    {
        $this->context = $context;
    }

    /**
     * @param $route
     * @param Data $data
     * @return Container
     */
    public abstract function render($route, Data $data);

    /**
     * @return string
     */
    public function getContext()
    {
        return $this->context;
    }

    /**
     * @param string $context
     */
    public function setContext($context)
    {
        $this->context = $context;
    }

}