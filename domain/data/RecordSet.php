<?php
/*
 -------------------------------------------------------------------
 | @project: apocalipse
 | @package: Apocalipse\Core\Domain
 | @file: RecordSet.php
 -------------------------------------------------------------------
 | @user: william 
 | @creation: 03/04/16 16:46
 | @copyright: gennesis.io / arraysoftware.net
 | @license: MIT
 -------------------------------------------------------------------
 | @description:
 | PHP class
 |
 */

namespace Apocalipse\Core\Domain\Data;


use Apocalipse\Core\Model\Type\Origin;
use \Iterator;

/**
 * Class RecordSet
 * @package Apocalipse\Core\Domain
 */
class RecordSet extends Origin implements Iterator
{
    /**
     * @var array
     */
    private $records = [];

    /**
     * @var
     */
    private $items;

    /**
     * RecordSet constructor.
     * @param $array
     * @param array $items
     */
    public function __construct($array, $items = [])
    {
        if (is_array($array)) {
            $this->records = $array;
        }
        $this->items = $items;
    }

    /**
     * @return int
     */
    public function size()
    {
        return count($this->records);
    }

    /**
     *
     */
    public function rewind()
    {
        reset($this->records);
    }

    /**
     * @return mixed
     */
    public function current()
    {
        $var = current($this->records);
        return new Record($var, true, $this->items);
    }

    /**
     * @return mixed
     */
    public function key()
    {
        $var = key($this->records);
        return $var;
    }

    /**
     * @return mixed
     */
    public function next()
    {
        $var = next($this->records);
        return $var;
    }

    /**
     * @return bool
     */
    public function valid()
    {
        $key = key($this->records);
        $var = ($key !== NULL && $key !== FALSE);
        return $var;
    }

    /**
     * @return array
     */
    public function getRecords()
    {
        return $this->records;
    }

}