<?php
/*
 -------------------------------------------------------------------
 | @project: apocalipse
 | @package: Apocalipse\Basic\Domain
 | @file: Dynamo.php
 -------------------------------------------------------------------
 | @user: william 
 | @creation: 06/04/16 14:20
 | @copyright: gennesis.io / arraysoftware.net
 | @license: MIT
 -------------------------------------------------------------------
 | @description:
 | PHP class
 |
 */

namespace Apocalipse\Core\Model;


use Apocalipse\Core\Domain\Definition\Collection;
use Apocalipse\Core\Domain\Definition\Field;
use Apocalipse\Core\Domain\Definition\Relation;
use Apocalipse\Core\Domain\ModelManager;
use Apocalipse\Core\Domain\Observer\Event;
use Apocalipse\Core\Flow\Wrapper;
use Apocalipse\Core\Model\Type\Scaffold;

/**
 * Class Dynamo
 * @package Apocalipse\Basic\Domain
 */
class Dynamo extends ModelManager
{
    /**
     * @var string
     */
    protected $module;

    /**
     * @var string
     */
    protected $entity;

    /**
     * @var Scaffold
     */
    protected $scaffold;

    /**
     * @var bool
     */
    protected $sleepy;

    /**
     * @var bool
     */
    protected $loaded = false;

    /**
     * Dynamo constructor.
     * @param string $module
     * @param string $entity
     * @param Collection $collection
     * @param string $driver
     */
    public function __construct($module, $entity, $collection = null, $driver = null)
    {
        parent::__construct($collection, $driver);

        if (is_null($collection)) {
            //Wrapper::info("Dynamo loaded without Collection or ID properties, this can be instable");
            $this->sleepy = true;
        }

        $this->module = $module;
        $this->entity = $entity;
    }

    /**
     * @return $this
     */
    public function skeleton()
    {
        /** @var Scaffold $scaffold */
        $scaffold = $this->getScaffold();

        if (!$this->loaded) {

            if ($scaffold->definition) {

                $definition = $scaffold->definition;

                if (isset($definition->driver) && $definition->driver) {

                    $this->setCollection(new Collection($definition->driver->collection, isset($definition->properties->id) ? $definition->properties->id : null));
                    $this->setDriver(isset($definition->driver->id) ? $definition->driver->id : null);

                    $definition->driver = null;

                } else {

                    Wrapper::err("Properties of DAO not loaded");
                }

                if (isset($definition->items) && is_array($definition->items)) {

                    foreach ($definition->items as $item) {

                        $name = $item->name;
                        $type = $item->type;

                        $required = isset($item->required) ? $item->required : true;
                        $unique = isset($item->unique) ? $item->unique : false;
                        $create = isset($item->create) ? $item->create :true;
                        $read = isset($item->read) ? $item->read :true;
                        $update = isset($item->update) ? $item->update :true;
                        $delete = isset($item->delete) ? $item->delete :true;
                        $length = isset($item->length) ? $item->length :null;
                        $behaviour = isset($item->behavior) ? $item->behavior : null;

                        $relationship = isset($item->relationship) ? new Relation($item->relationship) : null;
                        $component = isset($item->component) ? $item->component : null;
                        $default = isset($item->default) ? $item->default : null;
                        $nullable = isset($item->nullable) ? $item->nullable : null;

                        // Authority $authority, $name, $type,
                        // $required = false, $unique = false, $create = true,$read = true, $update = true, $delete = true, $length = null, $behaviour = null,
                        // $relationship = null, $component = null, $default = null, $nullable = false
                        $this->add(new Field($this, $name, $type,
                            $required, $unique, $create, $read, $update, $delete, $length,
                            $behaviour, $relationship, $component, $default, $nullable));
                    }

                    //Wrapper::info("Api instead by scaffold");
                } else {

                    Wrapper::err("Properties of Items not loaded");
                }

                if (isset($definition->listeners) && is_array($definition->listeners)) {

                    $listeners = iif($this->getListeners(), []);

                    foreach ($definition->listeners as $listener) {
                        $listeners[] = new Event($listener->on, $listener->actions, $listener->use, $listener->method);
                    }

                    $this->setListeners($listeners);
                }

                $this->loaded = true;
            } else {

                Wrapper::err("Definitions not loaded");
            }
        }

        return $this;
    }


    /**
     * @return string
     */
    public function getPathName()
    {
        // TODO: support environment
        return path(true, 'src', 'domain', 'scaffold', $this->module, $this->entity);
    }

    /**
     * @return object
     */
    public function getScaffold()
    {
        if (!$this->scaffold) {
            $this->scaffold = new Scaffold($this->getPathName());
        }

        return $this->scaffold;
    }

    /**
     * @param $method
     * @param $parameters
     * @return array|mixed
     */
    public function call($method, array $parameters)
    {
        $return = null;
        if (method_exists($this, $method)) {

            $return = call_user_func_array([$this, $method], $parameters);
        } else {

            Wrapper::push('Undefined method "' . $method . '"');
        }

        return $return;
    }

    /**
     * @return string
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * @param string $module
     */
    public function setModule($module)
    {
        $this->module = $module;
    }

    /**
     * @return string
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * @param string $entity
     */
    public function setEntity($entity)
    {
        $this->entity = $entity;
    }

}