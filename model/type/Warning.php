<?php
/*
 -------------------------------------------------------------------
 | @project: apocalipse
 | @package: Apocalipse\Core\Type
 | @file: Warnig.php
 -------------------------------------------------------------------
 | @user: william 
 | @creation: 17/03/16 04:46
 | @copyright: gennesis.io / arraysoftware.net
 | @license: MIT
 -------------------------------------------------------------------
 | @description:
 | PHP class
 |
 */

namespace Apocalipse\Core\Model\Type;


use \Exception;

/**
 * Class Warning
 * @package Apocalipse\Core\Type
 */
class Warning extends Exception
{
    /**
     * Warning constructor.
     * @param string $message
     * @param string $file
     * @param int $line
     */
    public function __construct($message, $file = "", $line = 0)
    {
        $this->message = gettype($message) === 'string' ? $message : print_r($message, true);
        $this->file = $file;
        $this->line = $line;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->getMessage() . ' on ' . $this->getFile() . ' at line ' . $this->getLine();
    }
}