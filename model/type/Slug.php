<?php
/*
 -------------------------------------------------------------------
 | @project: apocalipse
 | @package: Apocalipse\Core\Type
 | @file: Page.php
 -------------------------------------------------------------------
 | @user: william 
 | @creation: 17/03/16 01:13
 | @copyright: gennesis.io / arraysoftware.net
 | @license: MIT
 -------------------------------------------------------------------
 | @description:
 | PHP class
 |
 */

namespace Apocalipse\Core\Model\Type;

use stdClass;

class Slug extends Object
{
    /**
     * @var string
     */
    public $template = '';
    /**
     * @var string
     */
    public $controller = '';
    /**
     * @var string
     */
    public $brook = '';
    /**
     * @var string
     */
    public $cache = '';
    /**
     * @var array
     */
    public $parameters = [];

    /**
     * Page constructor.
     * @param stdClass $object
     */
    public function __construct($object = null)
    {
        if (is_object($object)) {
            $this->template = iif($object->template, '');
            $this->controller = iif($object->controller, '');
            $this->brook = iif($object->brook, false);
            $this->cache = iif($object->cache, false);
        }
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return json_encode($this);
    }

}