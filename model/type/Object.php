<?php

namespace Apocalipse\Core\Model\Type;

use IteratorAggregate;
use ArrayObject;

/**
 *
 * @noinspection PhpInconsistentReturnPointsInspection
 */
class Object implements IteratorAggregate
{
    /**
     * @var array
     */
    private $values = [];

    /**
     * Object constructor.
     * @param array $properties
     */
    public function __construct($properties = null)
    {
        $this->values = [];

        if (is_array($properties)) {

            $this->values = $properties;
        }
    }

    /**
     * @param $name
     * @param $value
     */
    public function __set($name, $value)
    {
        $this->values[$name] = $value;
    }

    /**
     * @param $name
     *
     * @return mixed
     */
    public function __get($name = null)
    {

        $value = null;

        switch ($name) {
            case 'json':
                $value = $this->__toString();
                break;
            case 'all':
                $value = $this->values;
                break;
            default:
                if (array_key_exists($name, $this->values)) {
                    $value = $this->values[$name];
                }
                break;
        }

        return $value;
    }

    /**
     * @param $name
     *
     * @return bool
     */
    public function __isset($name)
    {
        return isset($this->values[$name]);
    }

    /**
     * @param $name
     */
    public function __unset($name)
    {
        unset($this->values[$name]);
    }

    /**
     * @return mixed
     */
    public function getIterator()
    {
        $arrayObject = new ArrayObject($this->values);

        return $arrayObject->getIterator();
    }

    /**
     * @param $name
     * @param $args
     * @return bool|string
     */
    function __call($name, $args)
    {
        $return = false;

        switch ($name) {
            case 'json':
                $return = $this->__toString();
                break;
        }

        return $return;
    }

    /**
     * @return string
     */
    function __toString()
    {
        return json_encode($this->values);
    }

    /**
     * @param array $values
     */
    protected function setValues($values)
    {
        $this->values = $values;
    }

}