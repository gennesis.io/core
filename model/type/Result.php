<?php
/*
 -------------------------------------------------------------------
 | @project: apocalipse
 | @package: Apocalipse\Core\Type
 | @file: Result.php
 -------------------------------------------------------------------
 | @user: william 
 | @creation: 07/04/16 18:05
 | @copyright: fagoc.br / gennesis.io / arraysoftware.net
 | @license: MIT
 -------------------------------------------------------------------
 | @description:
 | PHP class
 |
 */

namespace Apocalipse\Core\Model\Type;


use Apocalipse\Core\Helper\Json;
use Apocalipse\Core\Flow\Wrapper;

/**
 * Class Result
 * @package Apocalipse\Core\Type
 */
class Result
{
    /**
     * @var boolean
     */
    private $ok;

    /**
     * @var string
     */
    private $status;

    /**
     * @var array
     */
    private $notifications;

    /**
     * @var mixed
     */
    private $data;

    /**
     * @var bool
     */
    private $closed;

    /**
     * @var array
     */
    private $blacklist;

    /**
     * Result constructor.
     * @param bool $ok
     * @param string $status
     * @param array $messages
     * @param mixed $data
     */
    public function __construct($ok = null, $status = null, array $messages = null, $data = null)
    {
        $this->ok = iif($ok, false);
        $this->status = iif($status, Wrapper::STATUS_UNDEFINED);
        $this->notifications = iif($messages, []);
        $this->data = $data;

        $this->blacklist = [Wrapper::STATUS_ERROR, Wrapper::STATUS_WARNING, Wrapper::STATUS_UNAUTHORIZED];
    }

    /**
     * @param $data
     */
    public function approve($data)
    {
        if (!$this->closed) {

            $this->data = $data;

            if (!in_array($this->status, $this->blacklist)) {
                $this->status = Wrapper::STATUS_SUCCESS;
            }
        } else {

            $this->push('Result is already closed');
        }
    }

    /**
     * @param $message
     * @param null $status
     * @param null $file
     * @param null $line
     */
    public function push($message, $status = null, $file = null, $line = null)
    {
        $message = $message ? $message : '[EMPTY]';

        $status = iif($status, Wrapper::STATUS_ERROR);

        if (in_array($status, $this->blacklist)) {
            $this->ok = false;
            $this->status = $status;
        }

        $this->notifications[] = new Notification($message, $status, $file, $line);

    }

    /**
     * @param $notifications
     */
    public function append($notifications)
    {
        /** @var Notification $notification */
        foreach ($notifications as $notification) {

            $this->push($notification->getMessage(), $notification->getStatus(), $notification->getFile(), $notification->getLine());
        }
    }

    /**
     * @return bool
     */
    public function close()
    {
        $this->closed = true;

        $this->ok = true;

        if (in_array($this->status, $this->blacklist)) {
            $this->ok = false;
        }
        return $this->ok;
    }

    /**
     * @return bool
     */
    public function isClosed()
    {
        return $this->closed;
    }

    /**
     * @param $key
     * @return mixed
     */
    public function get($key)
    {
        $value = isset($this->$key) ? ($this->$key) : null;

        if ($key === 'notifications') {
            $array = [];
            /** @var Notification $item */
            foreach ($value as $item) {
                if ($item instanceof Notification) {
                    $array[] = $item->expose();
                }
            }
            $value = $array;
        }

        return $value;
    }

    /**
     * @return boolean
     */
    public function isOk()
    {
        return $this->ok;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return array
     */
    public function getNotifications()
    {
        return $this->notifications;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @return array
     */
    public function getBlacklist()
    {
        return $this->blacklist;
    }

    /**
     * @return string
     */
    public function toJson()
    {
        $array = [];

        $properties = ['ok', 'status', 'data', 'notifications'];

        foreach ($properties as $key) {
            $array[$key] = $this->get($key);
        }
        return Json::encode($array);
    }

}